package eth

import (
	"context"
	"github.com/shopspring/decimal"
	"go.uber.org/zap"
	balance2 "test-case/runner"
)

type Provider struct {
	logger *zap.Logger
}

func NewProvider(logger *zap.Logger) *Provider {
	return &Provider{
		logger: logger,
	}
}

func (p *Provider) GetNextBalance(_ context.Context, addr *balance2.Address) (*balance2.Balance, error) {
	return &balance2.Balance{
		AddressID:   addr.ID,
		BlockNumber: 100500,
		Amount:      decimal.NewFromInt(100),
	}, nil
}
