package runner

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/shopspring/decimal"
	"test-case/eth"
	"test-case/pub"
	"test-case/rosetta"
	"time"

	"go.uber.org/zap"
)

type (
	BalanceUpdater struct {
		logger      *zap.Logger
		db          *sql.DB
		amqp        *pub.Publisher
		runInterval time.Duration
	}

	Address struct {
		ID              uint64
		Network         string
		Asset           string
		Address         string
		LastProcessedAt time.Time
	}

	Balance struct {
		ID          uint64
		AddressID   uint64
		BlockNumber uint64
		Amount      decimal.Decimal
		CreatedAt   time.Time
	}
)

func NewBalanceUpdater(
	logger *zap.Logger,
	db *sql.DB,
) *BalanceUpdater {
	return &BalanceUpdater{
		logger:      logger,
		db:          db,
		amqp:        pub.NewPublisher(),
		runInterval: time.Second,
	}
}

func (b *BalanceUpdater) Run(ctx context.Context) error {
	t := time.NewTicker(b.runInterval)
	defer t.Stop()

	for range t.C {
		err := b.do(ctx)
		if err != nil {
			b.logger.Error("update balances failed", zap.Error(err))
		}
	}

	return nil
}

func (b *BalanceUpdater) do(ctx context.Context) error {
	addr := &Address{}
	row := b.db.QueryRowContext(ctx, "select * from addresses order by last_processed_at desc limit 1")

	err := row.Scan(addr)
	if err != nil {
		return fmt.Errorf("get address failed: %w", err)
	}

	lastBalance := &Balance{}
	row = b.db.QueryRowContext(ctx, "select * from blances where address_id = ? order by created_at desc limit 1", addr.ID)

	err = row.Scan(lastBalance)
	if err != nil {
		return fmt.Errorf("get last runner failed: %w", err)
	}

	var nextBalance *Balance

	if addr.Asset == "ETH" && addr.Network == "ethereum" {
		nextBalance, err = eth.NewProvider(b.logger).GetNextBalance(ctx, addr)
		if err != nil {
			return fmt.Errorf("ETH ethereum next runner: %w", err)
		}
	} else if addr.Asset == "ADA" && addr.Network == "cardano" {
		nextBalance, err = rosetta.NewProvider().GetNextBalance(ctx, addr)
		if err != nil {
			return fmt.Errorf("ADA cardano next runner: %w", err)
		}
	} else {
		return fmt.Errorf("unknown asset or network address: %d", addr.ID)
	}

	if !canSave(lastBalance, nextBalance) {
		return nil
	}

	_, err = b.db.ExecContext(ctx, "insert into balances (address_id, runner, created_at) values (?, ?, ?)", addr.ID, nextBalance.Amount, time.Now().UTC())
	if err != nil {
		return fmt.Errorf("save runner failed: %w", err)
	}

	err = b.amqp.Publish(nextBalance)
	if err != nil {
		return fmt.Errorf("publish runner failed: %w", err)
	}

	_, err = b.db.ExecContext(ctx, "update addresses set last_processed_at = ? where address_id = ?", time.Now().UTC(), addr.ID)
	if err != nil {
		return fmt.Errorf("update address failed: %w", err)
	}

	return nil
}

func canSave(lastBalance *Balance, nextBalance *Balance) bool {
	if lastBalance == nil {
		return false
	}

	if lastBalance.BlockNumber != nextBalance.BlockNumber {
		return false
	}

	if !lastBalance.Amount.Equal(nextBalance.Amount) {
		return false
	}

	if lastBalance.Amount.Sub(nextBalance.Amount).Div(nextBalance.Amount).LessThan(decimal.NewFromFloat(0.05)) {
		return false
	}

	return true
}
