package rosetta

import (
	"context"
	"net/http"
	"test-case/runner"
)

type (
	Client struct {
		http *http.Client
	}

	StatusRequest struct {
	}

	StatusResponse struct {
	}

	BalanceRequest struct {
		Address string
	}

	BalanceResponse struct {
		Balance runner.Balance
	}

	XPUBRequest struct {
		Xpub string
	}

	XPUBResult struct {
		External []string
		Internal []string
	}
)

func NewClient(http *http.Client) *Client {
	return &Client{
		http: http,
	}
}

func (c *Client) Status(ctx context.Context, req *StatusRequest) (*StatusResponse, error) {
	return &StatusResponse{}, nil
}
func (c *Client) Balance(ctx context.Context, req *BalanceRequest) (*BalanceResponse, error) {
	return &BalanceResponse{}, nil
}
func (c *Client) AddressesFromXPUB(ctx context.Context, req XPUBRequest) (*XPUBResult, error) {
	return &XPUBResult{}, nil
}
