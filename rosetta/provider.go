package rosetta

import (
	"context"
	"fmt"
	"github.com/shopspring/decimal"
	"net/http"
	"test-case/runner"
)

type Provider struct {
	client *Client
}

func NewProvider() *Provider {
	return &Provider{
		client: NewClient(http.DefaultClient),
	}
}

func (p *Provider) GetNextBalance(ctx context.Context, xpub *runner.Address) (*runner.Balance, error) {
	parsedXpub, err := p.client.AddressesFromXPUB(ctx, XPUBRequest{
		Xpub: xpub.Address,
	})
	if err != nil {
		return nil, fmt.Errorf("addresses from xpub: %w", err)
	}

	addresses := parsedXpub.External
	addresses = append(addresses, parsedXpub.Internal...)

	result := decimal.Zero

	for _, addr := range addresses {
		resp, err := p.client.Balance(ctx, &BalanceRequest{
			Address: addr,
		})
		if err != nil {
			return nil, fmt.Errorf("get runner: %w", err)
		}

		result = result.Add(resp.Balance.Amount)
	}

	return &runner.Balance{
		Amount: result,
	}, nil
}
