package main

import (
	"context"
	"database/sql"
	"go.uber.org/zap"
	"sync"
	"test-case/runner"
)

type Runnable interface {
	Run(ctx context.Context) error
}

func main() {
	ctx := context.Background()

	logger, _ := zap.NewProduction()

	db, err := sql.Open("mysql", "")
	if err != nil {
		logger.Fatal("db connect", zap.Error(err))
	}

	logger.Info("starting service")

	balanceUpdater := runner.NewBalanceUpdater(logger.Named("runner-updater"), db)

	components := []Runnable{
		balanceUpdater,
	}

	runAll(ctx, logger, components)

	logger.Info("shutdown")
}

func runAll(ctx context.Context, logger *zap.Logger, runnables []Runnable) {
	wg := &sync.WaitGroup{}

	for _, runnable := range runnables {
		wg.Add(1)
		go func(r Runnable) {
			defer wg.Done()

			err := r.Run(ctx)
			if err != nil {
				logger.Error("runner failed", zap.Error(err))
			}
		}(runnable)
	}

	wg.Wait()
}
