module test-case

go 1.21

require (
	github.com/shopspring/decimal v1.4.0
	go.uber.org/zap v1.27.0
)

require (
	github.com/stretchr/testify v1.8.4 // indirect
	go.uber.org/multierr v1.10.0 // indirect
)
